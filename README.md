# README #

### What is this repository for? ###

* Yellow Messenger Assignment Chatbot

### How do I get set up? ###

* Follow the below steps
* clone the repository using command :- git clone https://Subhash1998@bitbucket.org/Subhash1998/yoyopizza.git
* python3 -m venv env
* source env/bin/activate
* pip install -r requirements.txt
* python manage.py runserver

### How do I get set up? ###

* Link :- https://yoyo-pizzas.herokuapp.com/

### Application Architecture Stepwise ###

* Bot asks for two options (in form of buttons) :- order pizza or track order
* If user clicks order pizza then next steps follow.
* user is asked for veg/non-veg pizza
* then user is shown the menu of store and user selects the pizza
* then user is asked for pizza size (small, medium, large)
* next, user is asked for quantity of pizza chosen.
* Finally user is asked details name, mobile number and address.
* All the details of the order and user who ordered are saved in database.
* server responds with orderId if order has been placed successfully else with appropriate error message.
* If option for tracking order is chosen then user is asked for orderId which was given while placing order.
* If orderId is correct, user is able to track it else he gets an error message.

### Technology Stack Used ###

* Python 3.6.9
* Django 3.0.8
* HTML
* CSS
* JavaScript