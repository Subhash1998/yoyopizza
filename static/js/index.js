var UIController = (function() {

    var DOMstrings = {
        chatArea: ".chat-area",
        sendBtn: ".input-send",
        orderBtn: ".order_btn",
        trackBtn: ".track_btn",
        vegBtn: ".veg_btn",
        nonvegBtn: ".nonveg_btn",
        vegtypeBtn: ".vegtype",
        nonvegtypeBtn: ".nonvegtype",
        inputMsg: ".input-message",
        smallBtn: ".small",
        mediumBtn: ".medium",
        largeBtn: ".large",
        one: ".one",
        two: ".two",
        three: ".three",
        four: ".four"
    };

    var Pizza = function(id, name, type, price, description) {
        this.id = id;
        this.name = name;
        this.type = type;
        this.price = price;
        this.description = description;
    };


    var data = {
        allPizza: {
            veg: [],
            nonveg: []
        }
    };

    return {

        addPizza: function(name, type, price, description) {
            var newPizza;

            //veg
            newPizza = new Pizza(1, "Veg Extravaganza", "veg", 450, "overload of golden corn, exotic black olives, crunchy onions, crisp capsicum, succulent mushrooms, juicyfresh tomatoes and jalapeno");
            data.allPizza["veg"].push(newPizza);
            newPizza = new Pizza(2, "Cloud 9", "veg", 350, "Onions,juicy tomatoes, crunchy baby corn, crisp capsicum, hot jalapeno and every vegetarian's first love: Paneer!");
            data.allPizza["veg"].push(newPizza);
            newPizza = new Pizza(3, "Peppy Paneer", "veg", 400, "Peppy paneer pizza has some mild bell peppers or capsicum. We have also thrown in some soft paneer making it a very desi pizza");
            data.allPizza["veg"].push(newPizza);

            //nonveg
            newPizza = new Pizza(4, "Non veg Supreme", "nonveg", 550, "Bite into supreme delight of Black Olives, Onions, Grilled Mushrooms, Pepper BBQ Chicken, Peri-Peri Chicken, Grilled Chicken Rashers");
            data.allPizza["nonveg"].push(newPizza);
            newPizza = new Pizza(5, "Florence Chicken Exotica", "nonveg", 650, "Italian Sausage in Butter Garlic Flavor, Barbecue Chicken and Jalapeno");
            data.allPizza["nonveg"].push(newPizza);
            newPizza = new Pizza(6, "Chicken Mexicana", "nonveg", 450, "Layers of rice, a creamy mixture of chicken and peppers and a Jack cheese topping make up the masterpiece that is Chicken Rice Mexicana!");
            data.allPizza["nonveg"].push(newPizza);

            return data;
        },

        getPizzafromID: function(id) {
            var myPizza;
            data.allPizza["veg"].forEach(function(cur){
                if(cur.id === id) {
                    myPizza = cur;
                }
            });

            data.allPizza["nonveg"].forEach(function(cur){
                if(cur.id === id) {
                    myPizza = cur;
                }
            });
            return myPizza;
        },

        getInput: function() {
            console.log("getting..");
            return document.querySelector(DOMstrings.inputMsg).value;
        },

        getDOMstrings: function() {
            return DOMstrings;
        }
    };
})();

var controller = (function(UICtrl) {

    var allPizzas, pizzaDetails, finalQuantity, tracking;

    tracking = false;

    var name, phone, address;

	var setupEventListeners = function() {

        var DOM = UICtrl.getDOMstrings();

        document.querySelector(DOM.orderBtn).addEventListener('click', orderPizza);

        document.querySelector(DOM.trackBtn).addEventListener('click', trackOrder);

        document.querySelector(DOM.sendBtn).addEventListener('click', store);

        document.addEventListener("keypress", function(event) {

            //console.log(event);
            if(event.keyCode == 13 || event.which == 13) { // which is for older browsers
                store();
                $("#bottom").focus();
            }

        });

        document.querySelector(DOM.chatArea).addEventListener('click', pizzaSize);        
    };

    var trackOrder = function() {

        tracking = true;

        var dom, html;

        dom = UICtrl.getDOMstrings();

        document.querySelector(dom.orderBtn).disabled = true;
        document.querySelector(dom.trackBtn).disabled = true;

        document.querySelector(dom.inputMsg).disabled = false;
        document.querySelector(dom.sendBtn).disabled = false;

        html = '<div class="bot-message">Enter your order ID in input box</div><br>';

        document.querySelector(dom.chatArea).insertAdjacentHTML('beforeend', html); 

        document.querySelector(dom.inputMsg).placeholder = "Enter your order ID";

    };

    var orderPizza = function() {

        var html, dom, orderbtn, trackbtn;

        dom = UICtrl.getDOMstrings();

        element = dom.chatArea;

        html = '<div class="user-input">Order Pizza</div><br><br>';

        document.querySelector(element).insertAdjacentHTML('beforeend', html);

        document.querySelector(dom.orderBtn).disabled = true;
        document.querySelector(dom.trackBtn).disabled = true;

        PizzaType();
    };

    var PizzaType = function() {

        var html, dom;

        dom = UICtrl.getDOMstrings();

        element = dom.chatArea;

        html = '<div class="bot-message">Select the type of Pizza..</div><br>';   

        document.querySelector(element).insertAdjacentHTML('beforeend', html);

        html = '<a href="#bottom"><button class="veg_btn" id="veg_btn" value="veg">Veg pizza</button><button class="nonveg_btn" id="nonveg" value="nonveg_btn">Non Veg Pizza</button><br></a>';

        document.querySelector(element).insertAdjacentHTML('beforeend', html);

        pizzaTypeEventListener();

    };

    var store = function() {

        var inputValue, html, dom, element;
        dom = UICtrl.getDOMstrings();
        inputValue = UICtrl.getInput();

        if(!tracking) {

            element = dom.chatArea;

            if(!name && inputValue) {
                name = inputValue;

                document.getElementById("message").value = "";
                html = '<div class="user-input"> ' + name + ' </div><br><br>';
                document.querySelector(element).insertAdjacentHTML('beforeend', html);

                document.getElementById("message").placeholder = "Enter your mobile number";

                html = '<div class="bot-message">Enter your mobile number</div><br>';
                document.querySelector(element).insertAdjacentHTML('beforeend', html);

            } else if(name && !phone && inputValue) {
                console.log(inputValue);
                var isNum = true;
                for (let i = inputValue.length - 1; i >= 0; i--) {
                    const d = inputValue.charCodeAt(i);
                    if (d < 48 || d > 57) {
                        isNum = false;
                    }
                }

                if(!isNum || inputValue.length !== 10) {

                    document.getElementById("message").value = "";

                    html = '<div class="bot-message">Enter a valid mobile number</div><br>';
                    document.querySelector(element).insertAdjacentHTML('beforeend', html);
                } else {
                    phone = inputValue;

                    document.getElementById("message").value = "";
                    html = '<div class="user-input"> ' + phone + ' </div><br><br>';
                    document.querySelector(element).insertAdjacentHTML('beforeend', html);

                    html = '<div class="bot-message">Enter your address</div><br>';
                    document.querySelector(element).insertAdjacentHTML('beforeend', html);

                    document.getElementById("message").placeholder = "Enter your address";
                }
            } else if(name && phone && !address && inputValue) {
                console.log("got address");
                address = inputValue;

                document.getElementById("message").value = "";
                document.querySelector(dom.inputMsg).disabled = true;
                document.querySelector(dom.sendBtn).disabled = true;

                html = '<div class="user-input"> ' + address + ' </div><br><br>';
                document.querySelector(element).insertAdjacentHTML('beforeend', html);

                var userDetails = {
                    name: name,
                    phone: phone,
                    address: address
                };

                pizzaDetails.price = finalQuantity*pizzaDetails.price;

                $.ajax({
                    url: "/orders/",
                    type: "POST",
                    data: JSON.stringify({'pizzaDetails': pizzaDetails, 'userDetails': userDetails}),
                    success: function( result ) {
                        if(result.success) {
                            html = '<div class="bot-message">Your order Id is ' + result.message + ' , it has been sent to you in your mobile number. Thanks for completing your order. (Payment on delivery)</div><br><br>';
                        } else {
                            html = '<div class="bot-message"> ' + result.message + ' </div><br><br>';
                        }
                        document.querySelector(dom.chatArea).insertAdjacentHTML('beforeend', html);
                    },
                    fail: function( result ){
                        html = '<div class="bot-message"> ' + result.message + ' </div><br><br>';
                        document.querySelector(dom.chatArea).insertAdjacentHTML('beforeend', html);
                    }
                });
            }
        } else if(inputValue) {

            html = '<div class="user-input"> ' + inputValue + ' </div><br><br>';
            document.querySelector(dom.chatArea).insertAdjacentHTML('beforeend', html);

            document.getElementById("message").value = "";
            
            $.ajax({
                url: "/track/",
                type: "POST",
                data: JSON.stringify({'inputOrderId': inputValue}),
                success: function( result ) {
                    html = '<div class="bot-message"> ' + result.message + ' </div><br><br>';
                    document.querySelector(dom.chatArea).insertAdjacentHTML('beforeend', html);
                },
                fail: function( result ){
                    html = '<div class="bot-message"> ' + result.message + ' </div><br><br>';
                    document.querySelector(dom.chatArea).insertAdjacentHTML('beforeend', html);
                }
            });
        }
    };

    var askforDetails = function() {

        console.log("asking details");
        var html, dom, element;

        dom = UICtrl.getDOMstrings();

        html = '<div class="bot-message">Enter your details please</div><br>';

        element = dom.chatArea;

        document.querySelector(element).insertAdjacentHTML('beforeend', html);

        html = '<div class="bot-message">Enter your name</div><br>';

        document.querySelector(element).insertAdjacentHTML('beforeend', html);

        var inputBox = document.getElementById("message");
        inputBox.placeholder = "Enter your name";
        inputBox.disabled = false;

        element = dom.sendBtn;
        document.querySelector(element).disabled = false;

        console.log("end"); 
    };

    var storeQuantity = function() {

        var htmlout, html, dom;

        dom = UICtrl.getDOMstrings();

        document.querySelector(dom.one).addEventListener('click', function() {
            html = '<div class="user-input">Selected quantity :- 1</div><br><br>';

            document.querySelector(element).insertAdjacentHTML('beforeend', html);

            finalQuantity = 1;

            document.querySelector(dom.one).disabled = true;
            document.querySelector(dom.two).disabled = true;
            document.querySelector(dom.three).disabled = true;
            document.querySelector(dom.four).disabled = true;

            askforDetails();
        });

        document.querySelector(dom.two).addEventListener('click', function() {
            html = '<div class="user-input">Selected quantity :- 2</div><br><br>';

            document.querySelector(element).insertAdjacentHTML('beforeend', html);

            finalQuantity = 2;

            document.querySelector(dom.one).disabled = true;
            document.querySelector(dom.two).disabled = true;
            document.querySelector(dom.three).disabled = true;
            document.querySelector(dom.four).disabled = true;

            askforDetails();
        });

        document.querySelector(dom.three).addEventListener('click', function() {
            html = '<div class="user-input">Selected quantity :- 3</div><br><br>';

            document.querySelector(element).insertAdjacentHTML('beforeend', html);

            finalQuantity = 3;

            document.querySelector(dom.one).disabled = true;
            document.querySelector(dom.two).disabled = true;
            document.querySelector(dom.three).disabled = true;
            document.querySelector(dom.four).disabled = true;

            askforDetails();
        });

        document.querySelector(dom.four).addEventListener('click', function() {
            html = '<div class="user-input">Selected quantity :- 4</div><br><br>';

            document.querySelector(element).insertAdjacentHTML('beforeend', html);

            finalQuantity = 4;

            document.querySelector(dom.one).disabled = true;
            document.querySelector(dom.two).disabled = true;
            document.querySelector(dom.three).disabled = true;
            document.querySelector(dom.four).disabled = true;

            askforDetails();
        });        

    };

    var askQuantity = function() {
        var htmlout, html, dom;

        dom = UICtrl.getDOMstrings();

        element = dom.chatArea;

        html = '<div class="bot-message">How many pizzas you want ?</div><br>';

        document.querySelector(element).insertAdjacentHTML('beforeend', html);

        html = '<a href="#bottom"><button class="one" id="one" value="1">1</button><button class="two" id="two" value="2">2</button><button class="three" id="three" value="3">3</button><button class="four" id="four" value="4">4</button><br></a>';

        document.querySelector(element).insertAdjacentHTML('beforeend', html);

        storeQuantity();

    };

    var storeSize = function() {

        var htmlout, html, dom;

        dom = UICtrl.getDOMstrings();

        document.querySelector(dom.smallBtn).addEventListener('click', function() {
            html = '<div class="user-input">Selected size :- Small</div><br><br>';

            document.querySelector(element).insertAdjacentHTML('beforeend', html);

            document.querySelector(dom.smallBtn).disabled = true;
            document.querySelector(dom.mediumBtn).disabled = true;
            document.querySelector(dom.largeBtn).disabled = true;

            askQuantity();
        });

        document.querySelector(dom.mediumBtn).addEventListener('click', function() {
            html = '<div class="user-input">Selected size :- Medium</div><br><br>';

            document.querySelector(element).insertAdjacentHTML('beforeend', html);

            document.querySelector(dom.smallBtn).disabled = true;
            document.querySelector(dom.mediumBtn).disabled = true;
            document.querySelector(dom.largeBtn).disabled = true;

            askQuantity();
        });

        document.querySelector(dom.largeBtn).addEventListener('click', function() {
            html = '<div class="user-input">Selected size :- Large</div><br><br>';

            document.querySelector(element).insertAdjacentHTML('beforeend', html);

            document.querySelector(dom.smallBtn).disabled = true;
            document.querySelector(dom.mediumBtn).disabled = true;
            document.querySelector(dom.largeBtn).disabled = true;

            askQuantity();
        });
    };

    var askSize = function() {
        var html, dom;

        dom = UICtrl.getDOMstrings();

        element = dom.chatArea;

        html = '<div class="bot-message">Select the size of pizza</div><br>';

        document.querySelector(element).insertAdjacentHTML('beforeend', html);

        html = '<a href="#bottom"><button class="small" id="small" value="small">Small</button><button class="medium" id="medium" value="medium">Medium</button><button class="large" id="large" value="large">Large</button><br></a>';

        document.querySelector(element).insertAdjacentHTML('beforeend', html);

        storeSize();
    };

    var pizzaSize = function(event) {

        var dom, itemID, ID, html;
        dom = UICtrl.getDOMstrings(); 

        itemID = event.target.id; 

        if(itemID && itemID>=1 && itemID<=6) {
            ID = parseInt(itemID);
            var obj = document.querySelector(".row");
            obj.remove();
            console.log(ID);

            pizzaDetails = UICtrl.getPizzafromID(ID);
            console.log(pizzaDetails);

            html = '<div class="user-input">' + pizzaDetails.name +'</div><br><br>';

            document.querySelector(element).insertAdjacentHTML('beforeend', html);

            askSize();

            // if(ID) {
            //     var details = askforDetails();
            // }

            // html = '<div class="bot-message">Great! Your order has been placed!!</div><br>';
            // document.querySelector(element).insertAdjacentHTML('beforeend', html);

            // html = '<div class="bot-message">Details :- Pizza : ' +  toBuy.name + ', Price : Rs.' + toBuy.price +'</div><br>';
            // document.querySelector(element).insertAdjacentHTML('beforeend', html);

            // html = '<div class="bot-message">You will get a confirmation call soon!</div><br>';
            // document.querySelector(element).insertAdjacentHTML('beforeend', html);
        }
    };

    var pizzaTypeEventListener = function() {

        var htmlout, html, dom, location;

        dom = UICtrl.getDOMstrings();

        document.querySelector(dom.vegBtn).addEventListener('click', function() {

            html = '<div class="user-input">Choose among available veg pizzas</div><br><br><br>';

            document.querySelector(element).insertAdjacentHTML('beforeend', html);

            vegPizza = allPizzas.allPizza["veg"];

            // html = "<div class='row'><div class='card'><img src='http://placehold.it/200x150'><p class='card-text'>This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p></div><div class='card'><img src='http://placehold.it/200x150'><p class='card-text'>This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p></div><div class='card'><img src='http://placehold.it/200x150'><p class='card-text'>This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p></div><div class='card'><img src='http://placehold.it/200x150'><p class='card-text'>This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p></div></div><br>";
            htmlout = "<div class='row'>";
            vegPizza.forEach(function(pizza){

                html = "<div class='card' id='%id%'><img src='%location%' class='pizza_image'/><p>" + pizza.name + "</p><p class='card-text'>"+ pizza.description + "</p><p><button class='vegtype' id='%id%'>Order Now</button></p></div>";
                location = '/media/images/' + pizza.name + '.jpg';
                html = html.replace('%location%',location);
                html = html.replace('%id%', pizza.id);
                html = html.replace('%id%',pizza.id);
                htmlout += html;
            });
            htmlout += '</div>';
            document.querySelector(dom.chatArea).insertAdjacentHTML('beforeend', htmlout);

            document.querySelector(dom.vegBtn).disabled = true;
            document.querySelector(dom.nonvegBtn).disabled = true;
            // pizzaSize();
        });

        document.querySelector(dom.nonvegBtn).addEventListener('click', function() {

            html = '<div class="user-input">Choose among available non-veg pizzas</div><br><br><br>';

            document.querySelector(element).insertAdjacentHTML('beforeend', html);

            nonvegPizza = allPizzas.allPizza["nonveg"];

            // html = "<div class='row'><div class='card'><img src='http://placehold.it/200x150'><p class='card-text'>This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p></div><div class='card'><img src='http://placehold.it/200x150'><p class='card-text'>This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p></div><div class='card'><img src='http://placehold.it/200x150'><p class='card-text'>This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p></div><div class='card'><img src='http://placehold.it/200x150'><p class='card-text'>This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p></div></div><br>";
            htmlout = "<div class='row'>";
            nonvegPizza.forEach(function(pizza){

                html = "<div class='card' id='card-%id%'><img src='%location%' class='pizza_image'/><p>" + pizza.name + "</p><p class='card-text'>"+ pizza.description + "</p><p><button class='nonvegtype' id='%id%'>Order Now</button></p></div>";
                location = '/media/images/' + pizza.name + '.jpg';
                html = html.replace('%location%',location);
                html = html.replace('%id%',pizza.id);
                html = html.replace('%id%',pizza.id);
                htmlout += html;
            });
            htmlout += '</div>';
            document.querySelector(dom.chatArea).insertAdjacentHTML('beforeend', htmlout);

            document.querySelector(dom.vegBtn).disabled = true;
            document.querySelector(dom.nonvegBtn).disabled = true;
            // document.querySelector(dom.nonvegtypeBtn).addEventListener('click',pizzaSize);
            // pizzaSize();
        });
        // pizzaSize();
        // document.querySelector(dom.orderBtn).addEventListener('click', pizzaSize(this.id));
    };

	return {

        init: function() {
            
            console.log('Application has started.');

            var html, element, dom, inputBox;

            allPizzas = UICtrl.addPizza();

            dom = UICtrl.getDOMstrings();

            element = dom.chatArea;

            html = '<div class="bot-message">Hello !! Welcome to Yo Yo Pizza</div><br>';
            
            document.querySelector(element).insertAdjacentHTML('beforeend', html);

            html = '<div class="bot-message">Tell us what you want to do ?</div><br>';

            document.querySelector(element).insertAdjacentHTML('beforeend', html);

            html = '<button class="order_btn">Order pizza</button><button class="track_btn">Track order</button><br>';

            document.querySelector(element).insertAdjacentHTML('beforeend', html);

            inputBox = document.getElementById("message");
            inputBox.placeholder = "Select from above.";
            inputBox.disabled = true;

            element = dom.sendBtn;

            document.querySelector(element).disabled = true;

            setupEventListeners();
        }
    };
})(UIController);

controller.init();