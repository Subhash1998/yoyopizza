from django.db import models

# Create your models here.

class UserDetail(models.Model):

	name = models.CharField(max_length=2000)
	phone = models.CharField(max_length=10)
	address = models.CharField(max_length=5000)

	def __str__(self):
		return str(self.name)


class OrderDetail(models.Model):

	pizzaName = models.CharField(max_length=100)
	pizzaType = models.CharField(max_length=10)
	pizzaPrice = models.FloatField()
	pizzaOrderId = models.CharField(max_length=1000)

	def __str__(self):
		return str(self.pizzaOrderId)


