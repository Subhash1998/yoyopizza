from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
import json
from .models import UserDetail, OrderDetail
import uuid

# Create your views here.
def home(request):
	return render(request,"index.html")

@csrf_exempt
def order(request):
	data = {}
	if request.method == 'POST':

		try:
			details = json.loads(request.body)
			userDetails = details["userDetails"]
			pizzaDetails = details["pizzaDetails"]
			generatedId = uuid.uuid1()
			print(generatedId)
			user = UserDetail.objects.create(name=userDetails["name"],
											phone=userDetails["phone"],
											address=userDetails["address"]
											)
			user.save()

			order = OrderDetail.objects.create(pizzaName=pizzaDetails["name"],
											pizzaType=pizzaDetails["type"],
											pizzaPrice=pizzaDetails["price"],
											pizzaOrderId=generatedId
											)
			order.save()

			data["success"] = True
			data["message"] = generatedId

		except Exception as e:

			data["success"] = False
			data["message"] = "Sorry! Could not process your request currently."

			return JsonResponse(data,safe=False)
	else:
		data["success"] = False
		data["message"] = "Method not allowed."

	return JsonResponse(data,safe=False)

@csrf_exempt
def track(request):
	data = {}

	if request.method == 'POST':
		try:
			details = json.loads(request.body)
			inputOrderId = details["inputOrderId"]

			pizza = OrderDetail.objects.filter(pizzaOrderId__iexact=inputOrderId)

			if pizza.count() == 0:
				data["success"] = True
				data["message"] = "You have provided wrong order id."
				return JsonResponse(data,safe=False)

			data["success"] = True
			data["message"] = "Your order is on the way. Will reach within 15 minutes."

			return JsonResponse(data,safe=False)

		except Exception as e:
			print(e)
			data["success"] = False
			data["message"] = "Error finding the order id. Try after sometime."

			print(data)
			return JsonResponse(data,safe=False)
	else:
		data["success"] = False
		data["message"] = "Method not allowed."

	return JsonResponse(data,safe=False)